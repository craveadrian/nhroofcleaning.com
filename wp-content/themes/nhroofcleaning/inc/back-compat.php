<?php
/**
 * NH Roof Cleaning back compat functionality
 *
 * Prevents NH Roof Cleaning from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since NH Roof Cleaning 1.0.0
 */

/**
 * Prevent switching to NH Roof Cleaning on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since NH Roof Cleaning 1.0.0
 */
function nhroofcleaning_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'nhroofcleaning_upgrade_notice' );
}
add_action( 'after_switch_theme', 'nhroofcleaning_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * NH Roof Cleaning on WordPress versions prior to 4.7.
 *
 * @since NH Roof Cleaning 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function nhroofcleaning_upgrade_notice() {
	$message = sprintf( __( 'NH Roof Cleaning requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'nhroofcleaning' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since NH Roof Cleaning 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function nhroofcleaning_customize() {
	wp_die(
		sprintf(
			__( 'NH Roof Cleaning requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'nhroofcleaning' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'nhroofcleaning_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since NH Roof Cleaning 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function nhroofcleaning_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'NH Roof Cleaning requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'nhroofcleaning' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'nhroofcleaning_preview' );
