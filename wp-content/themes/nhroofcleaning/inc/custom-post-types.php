<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'nhroofcleaning' ),
		'singular_name'			=> __( 'Fence', 'nhroofcleaning' ),
		'add_new'				=> __( 'New Fence', 'nhroofcleaning' ),
		'add_new_item'			=> __( 'Add New Fence', 'nhroofcleaning' ),
		'edit_item'				=> __( 'Edit Fence', 'nhroofcleaning' ),
		'new_item'				=> __( 'New Fence', 'nhroofcleaning' ),
		'view_item'				=> __( 'View Fence', 'nhroofcleaning' ),
		'search_items'			=> __( 'Search Fences', 'nhroofcleaning' ),
		'not_found'				=>  __( 'No Fences Found', 'nhroofcleaning' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'nhroofcleaning' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'nhroofcleaning' ),
		'singular_name'		=> __( 'Category', 'nhroofcleaning' ),
		'search_items'		=> __( 'Search Categories', 'nhroofcleaning' ),
		'all_items'			=> __( 'All Categories', 'nhroofcleaning' ),
		'edit_item'			=> __( 'Edit Category', 'nhroofcleaning' ),
		'update_item'		=> __( 'Update Category', 'nhroofcleaning' ),
		'add_new_item'		=> __( 'Add New Category', 'nhroofcleaning' ),
		'new_item_name'		=> __( 'New Category Name', 'nhroofcleaning' ),
		'menu_name'			=> __( 'Categories', 'nhroofcleaning' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );