<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'nhroofcleaning' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding-container container row">
			<?php //get_template_part( 'template-parts/header/site', 'branding' ); ?>
			<div class="col-lg-4">
				<div class="site-header__logo">
					<?php if ( has_custom_logo() ) : ?>
						<div class="hd-logo"><?php the_custom_logo(); ?></div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="site-header__nav">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		</div><!-- .layout-wrap -->

	</header><!-- #masthead -->

	<?php get_template_part( 'template-parts/header/site', 'banner' ); ?>

	<div id="content" class="site-content">
