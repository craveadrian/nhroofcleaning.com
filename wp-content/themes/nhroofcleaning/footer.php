<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-footer__top">
			<div class="row container container-1">
				<div class="site-footer__contact site-footer__details col-lg-8 container">
				<div class="col-md-4">
					<?php if ( checkoption( 'phone' ) ): 
						echo '<h6>Phone</h6>';
						echo do_shortcode('[nhroofcleaning_option var="phone" type="link" text="" link_type="phone" class="phone"]');
					endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( checkoption( 'email' ) ): 
						echo '<h6>Email</h6>';
						echo do_shortcode('[nhroofcleaning_option var="email" type="link" text="" link_type="email" class="email"]');
					endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( checkoption( 'address' ) ): 
						echo '<h6>Location</h6>';	
						echo '<a href="#" class="address">'. do_shortcode('[nhroofcleaning_option var="address" type="text"]').'</a>';
					endif; ?>
				</div>
				</div>
				<div class="site-footer__social site-footer__details col-lg-2">
					<h6>Follow Us</h6>
					<?php get_template_part( 'template-parts/navigation/navigation', 'social' ); ?>
				</div>
			</div>
		</div>
		<div class="site-footer__bottom">
			<div class="row container">
				<div class="site-footer__nav col-lg-7">
					<?php get_template_part( 'template-parts/navigation/navigation', 'bottom' ); ?>
				</div>
				<div class="site-footer__info col-xl-5">
					<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
					
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
