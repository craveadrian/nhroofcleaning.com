<p class="copyright">
	<?php
		$site_info = get_bloginfo( 'description' ) . ' - ' . get_bloginfo( 'name' ) . ' &copy; ' . date( 'Y' );

		if ( get_theme_mod( 'copyright' ) ) :
			echo get_theme_mod( 'copyright' );
		else :
			echo $site_info;
		endif;
	?>
</p>
<?php if ( get_theme_mod( 'silver_connect' ) ) : ?>

<p class="silver">

	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tech.png" alt="" class="company-logo" />

		Web Design
		Done by 
		Technodream Web Design

</p>

<?php endif; ?>