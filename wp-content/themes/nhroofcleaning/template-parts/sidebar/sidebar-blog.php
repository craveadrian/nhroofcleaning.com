<?php
/**
 * Displays the blog sidebar
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since 1.0.0
 */

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

	<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Sidebar', 'nhroofcleaning' ); ?>">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .widget-area -->

<?php endif; ?>
