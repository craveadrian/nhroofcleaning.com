<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if (is_sticky() && is_home() && !is_paged()) {
			printf('<span class="sticky-post">%s</span>', _x('Featured', 'post', 'nhroofcleaning'));
		}
		if (is_singular()) :
			the_title('<h1 class="entry-title">', '</h1>');
		else :
			the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>');
		endif;
		?>
	</header><!-- .entry-header -->

	<?php nhroofcleaning_post_thumbnail(); ?>

	<div class="entry-content">
		<?php if (is_single()) :
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'nhroofcleaning'),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __('Pages:', 'nhroofcleaning'),
					'after'  => '</div>',
				)
			);
		else :
			the_excerpt();
			?>
			<a href="<?php echo get_permalink() ?>" class="btn">READ MORE</a>
		<?php endif; ?>
	</div><!-- .entry-content -->

	<!-- <footer class="entry-footer">
		<?php //nhroofcleaning_entry_footer(); ?>
	</footer> -->
	<!-- .entry-footer -->
</article><!-- #post-${ID} -->