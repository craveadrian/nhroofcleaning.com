<?php
/**
 * Displays header site banner area
 *
 * @package WordPress
 * @subpackage NH_Roof_Cleaning
 * @since 1.0.0
 */
?>
<?php if ( get_theme_mod('banner_image') || get_theme_mod('banner_slider') || get_theme_mod('banner_social')) : ?>
<div class="site-banner">
	<div class="site-banner__background">
		<?php if( get_theme_mod('banner_slider') ):?>
		<?php else:?>
			<div><img src="<?php echo get_theme_mod('banner_image');?>" alt="Banner Image"></div>
		<?php endif;?>
	</div>
	<div class="site-social">
		<?php get_template_part( 'template-parts/navigation/navigation', 'social' ); ?>
	</div>
</div><!-- .site-banner -->
<?php endif; ?>